package theorem;

import com.sun.istack.internal.NotNull;

import java.util.HashMap;

public class Model {
    private HashMap<Variable, Double> values;

    public Model(@NotNull HashMap<Variable, Double> values) {
        this.values = values;
    }

    public HashMap<Variable, Double> getValues() {
        return values;
    }

    public String toString() {
        StringBuilder toReturn = new StringBuilder("{");
        for (Variable variable : values.keySet()) {
            toReturn.append(variable.getName()).append("=").append(values.get(variable)).append(", ");
        }
        toReturn = new StringBuilder(toReturn.substring(0, toReturn.length() - 2));
        toReturn.append("}");
        return toReturn.toString();
    }
}
