
%{

import java.io.*;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.sun.istack.internal.NotNull;
import theorem.Formula;
import theorem.Term;

%}

// tokens
%token eINVALIDTOKEN

%token FORALL EXISTS
%token <bval> TRUE FALSE

//%token <ival> cINT
%token <dval> cDouble

%token tID

// L-R associations
%left IMP
%left OR
%left AND
%left EQ NE
%left '<' LE '>' GE
%left '+' '-'
%left '*' '/'
%nonassoc NOT


%%

input: forall_variables exist_variables formula { cg.storeFormula($<form>3); } ;

forall_variables: FORALL input_variables '.'

input_variables: input_variable
               | input_variable ',' input_variables
               ;

input_variable: tID ':' tID { cg.declareVariable($<sval>1, $<sval>3, true); }
              | tID { cg.declareVariable($<sval>1, null, true); }
              ;

exist_variables: EXISTS output_variables '.'

output_variables: output_variable
                | output_variable ',' output_variables
                ;

output_variable: tID ':' tID { cg.declareVariable($<sval>1, $<sval>3, false); }
               | tID { cg.declareVariable($<sval>1, null, false); }
               ;

term: term '+' term   { $<term>$ = cg.mkFunctionTerm(Term.FunctionType.PLUS, $<term>1, $<term>3); }
    | term '-' term   { $<term>$ = cg.mkFunctionTerm(Term.FunctionType.MINUS, $<term>1, $<term>3); }
    | term '*' term   { $<term>$ = cg.mkFunctionTerm(Term.FunctionType.PRODUCT, $<term>1, $<term>3); }
    //| term '/' term   { $<term>$ = cg.mkFunctionTerm(Term.FunctionType.DIVIDE, $<term>1, $<term>3); }
    | '(' term ')'    { $<term>$ = $<term>2; }
    | variable        { $<term>$ = $<term>1; }
    | double_constant { $<term>$ = cg.mkDoubleConstantTerm($<dval>1); }
    ;

formula: formula OR formula  { $<form>$ = cg.mkFormula(Formula.FormulaType.OR, $<form>1, $<form>3); }
       | formula AND formula { $<form>$ = cg.mkFormula(Formula.FormulaType.AND, $<form>1, $<form>3); }
       | NOT formula         { $<form>$ = cg.mkFormula(Formula.FormulaType.NOT, $<form>2); }
       | term EQ term        { $<form>$ = cg.mkPredicate(Formula.PredicateType.EQ, $<term>1, $<term>3); }
       | term NE term        { $<form>$ = cg.mkPredicate(Formula.PredicateType.NE, $<term>1, $<term>3); }
       | term LE term        { $<form>$ = cg.mkPredicate(Formula.PredicateType.LE, $<term>1, $<term>3); }
       | term GE term        { $<form>$ = cg.mkPredicate(Formula.PredicateType.GE, $<term>1, $<term>3); }
       | term '<' term       { $<form>$ = cg.mkPredicate(Formula.PredicateType.LT, $<term>1, $<term>3); }
       | term '>' term       { $<form>$ = cg.mkPredicate(Formula.PredicateType.GT, $<term>1, $<term>3); }
       | '(' formula ')'     { $<form>$ = $<form>2; }
       | boolean_constant    { $<form>$ = cg.mkBooleanConstantFormula($<bval>1); }
       ;

variable: tID { $<term>$ = cg.getDeclaredVariableTerm($<sval>1); } ;

double_constant: cDouble ;

boolean_constant: TRUE | FALSE ;

%%

Lexer lexer;
CodeGenerator cg;

private String fileAddress;

private int yylex() {
    yylval = new Pval();    // indicates eof
    int yyl_ret = -1;
    try {
        yyl_ret = lexer.yylex();    // yylval will be set inside lexer.yylex()
    } catch (IOException e) {
        System.err.println("IOExeption: " + e);
    }
    return yyl_ret;
}

public void yyerror(String err) {
    switch (err) {
        case "syntax error":
        case "stack underflow. aborting...":
        case "Stack underflow. aborting...":
            cg.emitError(err);
            break;
        default:
            cg.setErrorPosition();
            break;
    }
}


public Parser(@NotNull Reader r) {
    this.lexer = new Lexer(r, this);
    this.cg = new CodeGenerator(this);

    //this.yydebug = true;
}

public Parser(@NotNull String fileAddress) throws FileNotFoundException {
    this(new FileReader(fileAddress));
    this.fileAddress = fileAddress;
}

//public Parser(Reader r, CodeGenerator cg) {
//    this.lexer = new Lexer(r, this);
//    this.cg = cg;
//
//    //this.yydebug = true;
//}

public Formula parse() {
    cg.parseStatus = this.yyparse();
    if (cg.hasErrors()) {
        throw new RuntimeException("There is some error in input.");
    }
    return cg.getFormula();
}
