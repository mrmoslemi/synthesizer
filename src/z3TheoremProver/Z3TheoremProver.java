package z3TheoremProver;

import com.microsoft.z3.*;
import com.sun.istack.internal.NotNull;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;

public class Z3TheoremProver implements theorem.TheoremProver {
    private Context context;
    private HashMap<theorem.Variable, Pair<Symbol, ArithExpr>> variables;
    private ArrayList<theorem.Variable> outputVariables;


    public Z3TheoremProver() {
        HashMap<String, String> configs = new HashMap<>();
        configs.put("model", "true");
        context = new Context(configs);
    }

    @Override
    public void setVariables(@NotNull ArrayList<theorem.Variable> variables) {
        this.variables = new HashMap<>();
        this.outputVariables = new ArrayList<>();
        for (theorem.Variable variable : variables) {
            Symbol symbol = context.mkSymbol(variable.getName());
            ArithExpr arithExpr = context.mkRealConst(symbol);
            this.variables.put(variable, new Pair<>(symbol, arithExpr));
            if (!variable.isInputVariable()) {
                outputVariables.add(variable);
            }
        }
    }

    @Override
    public theorem.Model getModel(@NotNull theorem.Formula formula) {
        Solver solver = context.mkSolver();
        BoolExpr boolExpr = getBoolExpr(formula);
        boolExpr = (BoolExpr) boolExpr.simplify();
        solver.add(boolExpr);
        if (solver.check() == Status.SATISFIABLE) {
            Model model = solver.getModel();
            return convertModel(model);
        } else {
            return null;
        }
    }

    @Override
    public boolean checkSat(@NotNull theorem.Formula formula) {
        Solver solver = context.mkSolver();
        BoolExpr boolExpr = getBoolExpr(formula);
        solver.add(boolExpr);
        return solver.check() == Status.SATISFIABLE;
    }

    @Override
    public theorem.Formula eliminateQuantifier(@NotNull theorem.Formula formula) {
        Quantifier quantifiedFormula = context.mkExists(
                getExistsExprs(),
                getBoolExpr(formula),
                1,
                null,
                null,
                null,
                null
        );

        Tactic tactic = context.mkTactic("qe");

        Goal goal = context.mkGoal(true, true, false);
        goal.add(quantifiedFormula);

        ApplyResult result = tactic.apply(goal);
        BoolExpr preCondition = result.getSubgoals()[0].AsBoolExpr();
        return convertBoolExpr(preCondition);
    }

    @Override
    public theorem.Formula simplify(theorem.Formula formula) {
        BoolExpr boolExpr = getBoolExpr(formula);
        boolExpr = (BoolExpr) boolExpr.simplify();
        return convertBoolExpr(boolExpr);
    }

    @Override
    public theorem.Term simplify(theorem.Term term) {
        ArithExpr arithExpr = getArithExpr(term);
        arithExpr = (ArithExpr) arithExpr.simplify();
        return convertArithExpr(arithExpr);
    }

    /**
     * @param formula to be converted to a BoolExpr
     * @return equivalence BoolExpr of given formula
     */
    @NotNull
    private BoolExpr getBoolExpr(@NotNull theorem.Formula formula) {
        theorem.Formula.FormulaType formulaType = formula.getFormulaType();
        BoolExpr[] booleanExpressions = null;
        theorem.Formula[] innerFormulas = formula.getFormulas();
        if (innerFormulas != null && innerFormulas.length > 0) {
            booleanExpressions = new BoolExpr[innerFormulas.length];
            for (int i = 0; i < innerFormulas.length; i++) {
                theorem.Formula innerFormula = innerFormulas[i];
                booleanExpressions[i] = getBoolExpr(innerFormula);
            }
        }
        switch (formulaType) {
            case TRUE:
                return context.mkTrue();
            case FALSE:
                return context.mkFalse();
            case NOT:
                if (booleanExpressions == null) {
                    throw new IllegalStateException();
                }
                return context.mkNot(booleanExpressions[0]);
            case AND:
                if (booleanExpressions == null) {
                    throw new IllegalStateException();
                }
                return context.mkAnd(booleanExpressions);
            case OR:
                if (booleanExpressions == null) {
                    throw new IllegalStateException();
                }
                return context.mkOr(booleanExpressions);
            case PREDICATE:
                theorem.Formula.PredicateType predicateType = formula.getPredicateType();
                ArithExpr operand0 = getArithExpr(formula.getTerms()[0]);
                ArithExpr operand1 = getArithExpr(formula.getTerms()[1]);
                switch (predicateType) {
                    case EQ:
                        return context.mkEq(operand0, operand1);
                    case LT:
                        return context.mkLt(operand0, operand1);
                    case LE:
                        return context.mkLe(operand0, operand1);
                    case GT:
                        return context.mkGt(operand0, operand1);
                    case GE:
                        return context.mkGe(operand0, operand1);
                    case NE:
                        return context.mkNot(context.mkEq(operand0, operand1));
                    default:
                        throw new IllegalStateException();
                }
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * @param term to be converted to a ArithExpr
     * @return equivalence ArithExpr of given term
     */
    @NotNull
    private ArithExpr getArithExpr(@NotNull theorem.Term term) {
        theorem.Term.TermType termType = term.getTermType();
        ArithExpr[] arithmeticExpressions = null;
        theorem.Term[] innerTerms = term.getTerms();
        if (innerTerms != null && innerTerms.length > 0) {
            arithmeticExpressions = new ArithExpr[innerTerms.length];
            for (int i = 0; i < innerTerms.length; i++) {
                theorem.Term innerTerm = innerTerms[i];
                arithmeticExpressions[i] = getArithExpr(innerTerm);
            }
        }
        switch (termType) {
            case CONST:
                return context.mkFPToReal(context.mkFP(term.getConstant(), context.mkFPSortDouble()));
            case VARIABLE:
                return getArithExpr(term.getVariable());
            case FUNCTION:
                theorem.Term.FunctionType functionType = term.getFunctionType();
                if (arithmeticExpressions == null) {
                    throw new IllegalStateException();
                }
                switch (functionType) {
                    case PLUS:
                        return context.mkAdd(arithmeticExpressions);
                    case MINUS:
                        return context.mkSub(arithmeticExpressions);
                    case PRODUCT:
                        return context.mkMul(arithmeticExpressions);
                    case DIVIDE:
                        return context.mkDiv(arithmeticExpressions[0], arithmeticExpressions[1]);
                    default:
                        throw new IllegalStateException();
                }
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * @param variable to be converted to a ArithExpr
     * @return equivalence ArithExpr of given variable
     */
    @NotNull
    private ArithExpr getArithExpr(@NotNull theorem.Variable variable) {
        return variables.get(variable).getValue();
    }

    @NotNull
    private Expr[] getExistsExprs() {
        ArrayList<Expr> toReturn = new ArrayList<>();
        for (theorem.Variable variable : outputVariables) {
            toReturn.add(getArithExpr(variable));
        }
        return toReturn.toArray(new Expr[0]);
    }

    private theorem.Model convertModel(Model model) {
        HashMap<theorem.Variable, Double> evaluation = new HashMap<>();
        FuncDecl[] functions = model.getConstDecls();
        for (FuncDecl function : functions) {
            Symbol symbol = function.getName();
            theorem.Variable variable = convertSymbol(symbol);
            Expr expr = model.getConstInterp(function);
            Double value = Double.parseDouble(expr.toString());
            evaluation.put(variable, value);
        }
        return new theorem.Model(evaluation);
    }

    private theorem.Variable convertSymbol(Symbol symbol) {
        for (theorem.Variable variable : variables.keySet()) {
            if (variables.get(variable).getKey().equals(symbol)) {
                return variable;
            }
        }
        throw new IllegalArgumentException("There is no variable in variable table that matches:" + symbol);
    }

    private theorem.Formula convertBoolExpr(BoolExpr boolExpr) {
        if (boolExpr.isTrue()) {
            return new theorem.Formula(theorem.Formula.FormulaType.TRUE);
        } else if (boolExpr.isFalse()) {
            return new theorem.Formula(theorem.Formula.FormulaType.FALSE);
        } else if (boolExpr.isAnd() || boolExpr.isOr() || boolExpr.isNot()) {
            Expr[] subExprs = boolExpr.getArgs();
            int argsLength = subExprs.length;
            theorem.Formula[] subFormulas = new theorem.Formula[argsLength];
            for (int i = 0; i < argsLength; i++) {
                subFormulas[i] = convertBoolExpr((BoolExpr) subExprs[i]);
            }
            if (boolExpr.isAnd()) {
                return new theorem.Formula(theorem.Formula.FormulaType.AND, subFormulas);
            } else if (boolExpr.isOr()) {
                return new theorem.Formula(theorem.Formula.FormulaType.OR, subFormulas);
            } else {
                return new theorem.Formula(theorem.Formula.FormulaType.NOT, subFormulas);
            }
        } else if (boolExpr.isEq() || boolExpr.isLT() || boolExpr.isLE() || boolExpr.isGT() || boolExpr.isGE() || boolExpr.isDistinct()) {
            Expr[] subExprs = boolExpr.getArgs();
            int argsLength = subExprs.length;
            theorem.Term[] subFormulas = new theorem.Term[argsLength];
            for (int i = 0; i < argsLength; i++) {
                subFormulas[i] = convertArithExpr((ArithExpr) subExprs[i]);
            }
            if (boolExpr.isEq()) {
                return new theorem.Formula(theorem.Formula.PredicateType.EQ, subFormulas[0], subFormulas[1]);
            } else if (boolExpr.isLT()) {
                return new theorem.Formula(theorem.Formula.PredicateType.LT, subFormulas[0], subFormulas[1]);
            } else if (boolExpr.isLE()) {
                return new theorem.Formula(theorem.Formula.PredicateType.LT, subFormulas[0], subFormulas[1]);
            } else if (boolExpr.isGT()) {
                return new theorem.Formula(theorem.Formula.PredicateType.GT, subFormulas[0], subFormulas[1]);
            } else if (boolExpr.isGE()) {
                return new theorem.Formula(theorem.Formula.PredicateType.GE, subFormulas[0], subFormulas[1]);
            } else {
                return new theorem.Formula(theorem.Formula.PredicateType.NE, subFormulas[1], subFormulas[1]);
            }
        } else {
            return null;
        }
    }

    private theorem.Term convertArithExpr(ArithExpr arithExpr) {
        if (arithExpr.isNumeral()) {
            return new theorem.Term(Double.parseDouble(arithExpr.toString()));
        } else if (arithExpr.isConst() || arithExpr.isFuncDecl()) {
            String name = arithExpr.toString();
            theorem.Variable variable = null;
            for (theorem.Variable var : variables.keySet()) {
                if (var.getName().equals(name)) {
                    variable = var;
                }
            }
            if (variable == null) {
                throw new IllegalStateException("variable name is invalid:" + name);
            }
            return new theorem.Term(variable);
        } else if (arithExpr.isAdd() || arithExpr.isSub() || arithExpr.isMul()) {
            Expr[] subExprs = arithExpr.getArgs();
            int argsLength = subExprs.length;
            theorem.Term[] subFormulas = new theorem.Term[argsLength];
            for (int i = 0; i < argsLength; i++) {
                subFormulas[i] = convertArithExpr((ArithExpr) subExprs[i]);
            }
            if (arithExpr.isAdd()) {
                return new theorem.Term(theorem.Term.FunctionType.PLUS, subFormulas);
            } else if (arithExpr.isSub()) {
                return new theorem.Term(theorem.Term.FunctionType.MINUS, subFormulas);
            } else {
                return new theorem.Term(theorem.Term.FunctionType.PRODUCT, subFormulas);
            }
        } else {
            throw new IllegalArgumentException("invalid argument for convertArithExpr method:" + arithExpr.toString());
        }
    }
}
