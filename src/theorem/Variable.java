package theorem;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

public class Variable {
    private static HashMap<String, Variable> variableTable;

    static {
        variableTable = new HashMap<>();
    }

    private String name;
    private boolean inputVariable;

    public Variable(@NotNull String name, boolean inputVariable) {
        if (variableTable.containsKey(name)) {
            throw new IllegalArgumentException("theorem.Variable name <" + name + "> is used");
        }
        this.name = name;
        this.inputVariable = inputVariable;
        variableTable.put(this.name, this);
    }

    public String getName() {
        return this.name;
    }

    public boolean isInputVariable() {
        return this.inputVariable;
    }

    public static ArrayList<Variable> getVariableTable() {
        return new ArrayList<>(Variable.variableTable.values());
    }

    @Override
    public String toString() {
        return name;
    }
    @Override
    public boolean equals(Object o){
        if (o instanceof Variable){
            return this.getName().equals(((Variable)o).getName());
        }else{
            return false;
        }
    }
}
