package theorem;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;

public class Formula {
    public static Formula generateCondjuction(Formula[] formulas) {
        if (formulas.length == 0) {
            return new Formula(Formula.FormulaType.TRUE);
        } else if (formulas.length == 1) {
            return formulas[0];
        } else {
            return new Formula(Formula.FormulaType.AND, formulas);
        }
    }

    public boolean containsVariable(Variable variable) {
        for (Term term : terms) {
            if (term.containsVariable(variable)) {
                return true;
            }
        }
        return false;
    }

    public boolean isCompatibleWith(Model model) {
        double firstTerm = terms[0].calculateWith(model);
        double secondTerm = terms[1].calculateWith(model);
        switch (this.predicateType) {
            case EQ:
                return firstTerm == secondTerm;
            case LT:
                return firstTerm < secondTerm;
            case LE:
                return firstTerm <= secondTerm;
            case GT:
                return firstTerm > secondTerm;
            case GE:
                return firstTerm >= secondTerm;
            case NE:
                return firstTerm != secondTerm;
        }
        return false;
    }

    public Formula circulateOn(Variable variable) {
        //TODO
        return this;
    }

    public enum FormulaType {
        TRUE, FALSE, AND, OR, NOT, PREDICATE,
    }

    public enum PredicateType {
        EQ, LT, LE, GT, GE, NE
    }

    private FormulaType formulaType;
    private PredicateType predicateType;
    private Term[] terms;
    private Formula[] formulas;

    public Formula(@NotNull FormulaType formulaType, Formula... formulas) {
        switch (formulaType) {
            case TRUE:
            case FALSE:
                if (formulas != null && formulas.length > 0) {
                    throw new IllegalArgumentException("Illegal number of arguments");
                }
                break;
            case AND:
            case OR:
                if (formulas == null || formulas.length < 2) {
                    throw new IllegalArgumentException("Illegal number of arguments");
                }
                break;
            case NOT:
                if (formulas == null || formulas.length != 1) {
                    throw new IllegalArgumentException("Negate formula can have 1 argument");
                }
                break;
            case PREDICATE:
                throw new IllegalArgumentException("Predicate can't be applied to formulas");
        }
        this.formulaType = formulaType;
        this.formulas = formulas;
    }

    public Formula(@NotNull PredicateType predicateType, @NotNull Term t0, @NotNull Term t1) {
        this.formulaType = FormulaType.PREDICATE;
        this.predicateType = predicateType;
        this.terms = new Term[2];
        this.terms[0] = t0;
        this.terms[1] = t1;
    }

    public FormulaType getFormulaType() {
        return this.formulaType;
    }

    public PredicateType getPredicateType() {
        return this.predicateType;
    }

    public Term[] getTerms() {
        return this.terms;
    }

    public Formula[] getFormulas() {
        return this.formulas;
    }

    @Override
    public String toString() {
        StringBuilder toReturn = new StringBuilder();
        switch (formulaType) {
            case TRUE:
                return "true";
            case FALSE:
                return "false";
            case AND:
                toReturn.append("(");
                for (int i=0;i<formulas.length;i++) {
                    Formula formula = formulas[i];
                    toReturn.append(formula.toString());
                    if(i!=formulas.length-1){
                        toReturn.append(" and ");
                    }
                }
                toReturn.append(")");
                return toReturn.toString();
            case OR:
                toReturn.append("(");
                for (int i=0;i<formulas.length;i++) {
                    Formula formula = formulas[i];
                    toReturn.append(formula.toString());
                    if(i!=formulas.length-1){
                        toReturn.append(" or ");
                    }
                }
                toReturn.append(")");
                return toReturn.toString();
            case NOT:
                toReturn.append("(not ").append(formulas[0].toString()).append(")");
                return toReturn.toString();
            case PREDICATE:
                toReturn.append("(").append(terms[0].toString());
                switch (predicateType) {
                    case EQ:
                        toReturn.append(" == ");
                        break;
                    case LT:
                        toReturn.append(" < ");
                        break;
                    case LE:
                        toReturn.append(" <= ");
                        break;
                    case GT:
                        toReturn.append(" > ");
                        break;
                    case GE:
                        toReturn.append(" >= ");
                        break;
                    case NE:
                        toReturn.append(" != ");
                        break;
                }
                toReturn.append(terms[1].toString()).append(")");
                return toReturn.toString();
            default:
                return null;
        }
    }

    public ArrayList<Formula> getLiterals() {
        ArrayList<Formula> toReturn = new ArrayList<>();
        switch (this.formulaType) {
            case TRUE:
                return toReturn;
            case FALSE:
                return toReturn;
            case PREDICATE:
                toReturn.add(this);
                return toReturn;
            case NOT:
            case AND:
            case OR:
                for (Formula formula : formulas) {
                    toReturn.addAll(formula.getLiterals());
                }
                return toReturn;
        }
        throw new IllegalStateException("Invalid formula type:" + this.formulaType);
    }
}
