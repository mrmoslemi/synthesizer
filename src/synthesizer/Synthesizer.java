package synthesizer;

import theorem.*;
import z3TheoremProver.Z3TheoremProver;

import parser.Parser;

import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Synthesizer {
    private Parser parser;
    private TheoremProver theoremProver;
    private Program program;
    private HashMap<theorem.Variable, ArrayList<theorem.Formula>> variableLiterals;


    public Synthesizer(String[] args) {
        if (args == null || args.length == 0) {
            System.err.println("No input file is given.");
            return;
        }
        String inputFileAddress = args[0];
        try {
            this.parser = new Parser(inputFileAddress);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            throw new RuntimeException();
        }
        this.theoremProver = new Z3TheoremProver();
        this.program = new Program(this.theoremProver);
    }

    public void run() {
        Formula formula = parser.parse();
        theoremProver.setVariables(Variable.getVariableTable());
        setBaseLiterals(formula);
        while (true) {
            if (program.isComplete()) {
                break;
            }
            Formula preConditions = program.getPreConditions();
            Formula tempFormula = new Formula(Formula.FormulaType.AND, formula, new Formula(Formula.FormulaType.NOT, preConditions));
            Model model = theoremProver.getModel(tempFormula);
            if (model == null) {
                System.out.println("Input specification is not valid");
                System.exit(0);
                break;
            } else {
                ProgramDecomposition decomposedPart = modelBasedProject(model);
                program.addProgramDecomposition(decomposedPart);
            }
        }
        if (program.isComplete()) {
            System.out.println(program);
        }
    }

    private void setBaseLiterals(Formula formula) {
        variableLiterals = new HashMap<>();
        for (theorem.Variable variable : Variable.getVariableTable()) {
            if (!variable.isInputVariable()) {
                ArrayList<Formula> includedLiterals = new ArrayList<>();
                for (Formula literal : formula.getLiterals()) {
                    if (literal.containsVariable(variable)) {
                        includedLiterals.add(literal);
                    }
                }
                variableLiterals.put(variable, includedLiterals);
            }
        }
    }

    /**
     * @param variable is a variable to get skolem constraint for
     * @param model    is a model to use for skolem constraint generation
     * @return a set of all literals that contain such variable and are compatible with this model
     */
    @NotNull
    private Formula[] getSkolemConstraints(@NotNull Variable variable, @NotNull Model model) {
        ArrayList<Formula> skolemConstraints = new ArrayList<>();
        for (Formula literal : variableLiterals.get(variable)) {
            if (literal.isCompatibleWith(model)) {
                skolemConstraints.add(literal);
            }
        }
        return skolemConstraints.toArray(new Formula[0]);
    }

    /**
     * @param model a valid model for this formula
     * @return uses quantifier elimination and skolemTermGenerator to generate a ProgramDecomposition
     */
    @NotNull
    private ProgramDecomposition modelBasedProject(@NotNull Model model) {
        ArrayList<Formula> preConditionConstraints = new ArrayList<>();
        ArrayList<SkolemTerm> skolemTerms = new ArrayList<>();
        for (Variable variable : variableLiterals.keySet()) {
            Formula[] skolemConstraints = getSkolemConstraints(variable, model);

            Formula toEliminateFormula = Formula.generateCondjuction(skolemConstraints);
            Formula eliminatedFormula = theoremProver.eliminateQuantifier(toEliminateFormula);
            preConditionConstraints.add(eliminatedFormula);

            Term term = generateSkolemTerm(variable, skolemConstraints);
            SkolemTerm skolemTerm = new SkolemTerm(variable, term);
            skolemTerms.add(skolemTerm);
        }
        Formula preCondition = Formula.generateCondjuction(preConditionConstraints.toArray(new Formula[0]));
        preCondition = theoremProver.simplify(preCondition);
        return new ProgramDecomposition(preCondition, skolemTerms.toArray(new SkolemTerm[0]));
    }

    private Term generateSkolemTerm(Variable variable, Formula[] skolemConstraints) {
        ArrayList<Term> E = new ArrayList<>();
        ArrayList<Term> N = new ArrayList<>();
        ArrayList<Term> L = new ArrayList<>();
        ArrayList<Term> U = new ArrayList<>();
        for (Formula constraint : skolemConstraints) {
            if (constraint.getFormulaType() != Formula.FormulaType.PREDICATE) {
                throw new IllegalStateException("skolem constraint should be a predicate:" + constraint);
            } else {
                Formula circulatedConstraint = constraint.circulateOn(variable);
                Term value = circulatedConstraint.getTerms()[1];
                switch (circulatedConstraint.getPredicateType()) {
                    case EQ:
                        E.add(value);
                        break;
                    case NE:
                        N.add(value);
                        break;
                    case LE:
                        U.add(value);
                        break;
                    case LT:
                        U.add(value);
                        N.add(value);
                        break;
                    case GE:
                        L.add(value);
                        break;
                    case GT:
                        L.add(value);
                        N.add(value);
                }
            }
        }
        if (!E.isEmpty()) {
            return E.get(0);
        }
        if (L.isEmpty()) {
            return new Term(Double.MIN_VALUE);
        }
        if (U.isEmpty()) {
            return new Term(Double.MAX_VALUE);
        }
        return new Term(0.0);
    }

}
