package synthesizer;

import theorem.Formula;

public class ProgramDecomposition {
    private Formula preCondition;
    private SkolemTerm[] skolemTerms;

    ProgramDecomposition(Formula preCondition, SkolemTerm[] skolemTerms) {
        this.preCondition = preCondition;
        this.skolemTerms = skolemTerms;
    }

    Formula getPreCondition() {
        return this.preCondition;
    }

    public String toString() {
        StringBuilder toReturn = new StringBuilder("if (" + preCondition.toString() + ") then (\n");
        for (SkolemTerm skolemTerm : skolemTerms) {
            toReturn.append("\t").append(skolemTerm.toString()).append("\n");
        }
        toReturn.append("\treturn\n)");
        return toReturn.toString();
    }
}
