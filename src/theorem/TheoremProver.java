package theorem;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.ArrayList;

public interface TheoremProver {
    /**
     * @param variables passes a set of variables to theoremProver
     */
    void setVariables(@NotNull ArrayList<Variable> variables);

    /**
     * @param formula is a nonNull formula to get a satisfying model
     * @return a satisfying model of formula. returns null if formula is UNSAT
     */
    @Nullable
    Model getModel(@NotNull Formula formula);

    /**
     * @param formula is a nonNull formula to check if it's satisfiable
     * @return true if formula is SAT, false if it's UNSAT
     */
    boolean checkSat(@NotNull Formula formula);

    /**
     * @param formula an input formula
     * @return eliminates outputVariables from formula, and returns a quantifier free and outputVariable free formula
     */
    @NotNull
    Formula eliminateQuantifier(@NotNull Formula formula);

    /**
     * @param formula input formula
     * @return simplified formula
     */
    @NotNull
    Formula simplify(@NotNull Formula formula);

    /**
     * @param term input term
     * @return simplified term
     */
    @NotNull
    Term simplify(@NotNull Term term);
}
