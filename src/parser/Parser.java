//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";



package parser;



//#line 3 "Parser.y"

import java.io.*;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.sun.istack.internal.NotNull;
import theorem.Formula;
import theorem.Term;

//#line 27 "Parser.java"




public class Parser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//## **user defined:Pval
String   yytext;//user variable to return contextual strings
Pval yyval; //used to return semantic vals from action routines
Pval yylval;//the 'lval' (result) I got from yylex()
Pval valstk[] = new Pval[YYSTACKSIZE];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
final void val_init()
{
  yyval=new Pval();
  yylval=new Pval();
  valptr=-1;
}
final void val_push(Pval val)
{
  try {
    valptr++;
    valstk[valptr]=val;
  }
  catch (ArrayIndexOutOfBoundsException e) {
    int oldsize = valstk.length;
    int newsize = oldsize*2;
    Pval[] newstack = new Pval[newsize];
    System.arraycopy(valstk,0,newstack,0,oldsize);
    valstk = newstack;
    valstk[valptr]=val;
  }
}
final Pval val_pop()
{
  return valstk[valptr--];
}
final void val_drop(int cnt)
{
  valptr -= cnt;
}
final Pval val_peek(int relative)
{
  return valstk[valptr-relative];
}
final Pval dup_yyval(Pval val)
{
  return val;
}
//#### end semantic value section ####
public final static short eINVALIDTOKEN=257;
public final static short FORALL=258;
public final static short EXISTS=259;
public final static short TRUE=260;
public final static short FALSE=261;
public final static short cDouble=262;
public final static short tID=263;
public final static short IMP=264;
public final static short OR=265;
public final static short AND=266;
public final static short EQ=267;
public final static short NE=268;
public final static short LE=269;
public final static short GE=270;
public final static short NOT=271;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    1,    4,    4,    5,    5,    2,    6,    6,    7,
    7,    8,    8,    8,    8,    8,    8,    3,    3,    3,
    3,    3,    3,    3,    3,    3,    3,    3,    9,   10,
   11,   11,
};
final static short yylen[] = {                            2,
    3,    3,    1,    3,    3,    1,    3,    1,    3,    3,
    1,    3,    3,    3,    3,    1,    1,    3,    3,    2,
    3,    3,    3,    3,    3,    3,    3,    1,    1,    1,
    1,    1,
};
final static short yydefred[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    2,
    0,    0,    0,    0,   31,   32,   30,   29,    0,    0,
    0,    0,   16,   17,   28,    5,    4,    0,    7,    0,
   20,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   10,    9,   27,   15,    0,   19,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   14,
    0,
};
final static short yydgoto[] = {                          2,
    3,    8,   21,    5,    6,   13,   14,   22,   23,   24,
   25,
};
final static short yysindex[] = {                      -254,
 -252,    0, -241,  -38,  -24,  -20, -237,  -40, -235,    0,
 -252,  -28,  -13,    3,    0,    0,    0,    0,  -40,  -40,
 -183,  -11,    0,    0,    0,    0,    0, -225,    0, -237,
    0,  -39,  -33,  -40,  -40,  -34,  -34,  -34,  -34,  -34,
  -34,  -34,  -34,  -34,    0,    0,    0,    0, -221,    0,
  -34,   -2,   -2,   -2,   -2,   -2,   -2,   11,   11,    0,
   -6,
};
final static short yyrindex[] = {                         0,
    0,    0,    0,  -41,    0,   22,    0,    0,    0,    0,
    0,  -30,    0,   24,    0,    0,    0,    0,    0,    0,
   57,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,   25,    0,
    0,   13,   15,   17,   19,   21,   23,    1,    7,    0,
    0,
};
final static short yygindex[] = {                         0,
    0,    0,   61,   48,    0,   54,    0,   35,    0,    0,
    0,
};
final static int YYTABLESIZE=290;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         20,
   12,   47,    6,    1,    6,   51,   13,   48,   44,   42,
    4,   43,   21,   11,   22,   11,   25,    7,   23,    9,
   26,   10,   24,   11,   18,   12,   38,   26,   40,   28,
   44,   42,   29,   43,   48,   44,   42,   45,   43,   44,
   42,   12,   43,   12,   35,   12,   30,   13,   38,   13,
   40,   13,   44,   21,   33,   22,    1,   25,   27,   23,
   12,   26,   12,   24,    0,   18,   13,    3,   13,    8,
   52,   53,   54,   55,   56,   57,   58,   59,   60,   31,
   32,   34,   35,   46,    0,   61,    0,    0,    0,    0,
    0,    0,    0,    0,   49,   50,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   15,
   16,   17,   18,    0,    0,   34,   35,   17,   18,    0,
   19,    0,    0,   36,   37,   39,   41,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,   36,   37,   39,   41,    0,
    0,    0,    0,    0,    0,   12,   12,   12,   12,   12,
   12,   13,   13,   13,   13,   13,   13,   21,   21,   22,
   22,   25,   25,   23,   23,   26,   26,   24,   24,   18,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         40,
    0,   41,   44,  258,   46,   40,    0,   41,   42,   43,
  263,   45,    0,   44,    0,   46,    0,  259,    0,   58,
    0,   46,    0,   44,    0,  263,   60,  263,   62,   58,
   42,   43,   46,   45,   41,   42,   43,  263,   45,   42,
   43,   41,   45,   43,  266,   45,   44,   41,   60,   43,
   62,   45,   42,   41,   20,   41,    0,   41,   11,   41,
   60,   41,   62,   41,   -1,   41,   60,   46,   62,   46,
   36,   37,   38,   39,   40,   41,   42,   43,   44,   19,
   20,  265,  266,   30,   -1,   51,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   34,   35,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  260,
  261,  262,  263,   -1,   -1,  265,  266,  262,  263,   -1,
  271,   -1,   -1,  267,  268,  269,  270,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  267,  268,  269,  270,   -1,
   -1,   -1,   -1,   -1,   -1,  265,  266,  267,  268,  269,
  270,  265,  266,  267,  268,  269,  270,  265,  266,  265,
  266,  265,  266,  265,  266,  265,  266,  265,  266,  265,
};
}
final static short YYFINAL=2;
final static short YYMAXTOKEN=271;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,"'('","')'","'*'","'+'","','",
"'-'","'.'","'/'",null,null,null,null,null,null,null,null,null,null,"':'",null,
"'<'",null,"'>'",null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,"eINVALIDTOKEN","FORALL","EXISTS","TRUE","FALSE",
"cDouble","tID","IMP","OR","AND","EQ","NE","LE","GE","NOT",
};
final static String yyrule[] = {
"$accept : input",
"input : forall_variables exist_variables formula",
"forall_variables : FORALL input_variables '.'",
"input_variables : input_variable",
"input_variables : input_variable ',' input_variables",
"input_variable : tID ':' tID",
"input_variable : tID",
"exist_variables : EXISTS output_variables '.'",
"output_variables : output_variable",
"output_variables : output_variable ',' output_variables",
"output_variable : tID ':' tID",
"output_variable : tID",
"term : term '+' term",
"term : term '-' term",
"term : term '*' term",
"term : '(' term ')'",
"term : variable",
"term : double_constant",
"formula : formula OR formula",
"formula : formula AND formula",
"formula : NOT formula",
"formula : term EQ term",
"formula : term NE term",
"formula : term LE term",
"formula : term GE term",
"formula : term '<' term",
"formula : term '>' term",
"formula : '(' formula ')'",
"formula : boolean_constant",
"variable : tID",
"double_constant : cDouble",
"boolean_constant : TRUE",
"boolean_constant : FALSE",
};

//#line 89 "Parser.y"

Lexer lexer;
CodeGenerator cg;

private String fileAddress;

private int yylex() {
    yylval = new Pval();    // indicates eof
    int yyl_ret = -1;
    try {
        yyl_ret = lexer.yylex();    // yylval will be set inside lexer.yylex()
    } catch (IOException e) {
        System.err.println("IOExeption: " + e);
    }
    return yyl_ret;
}

public void yyerror(String err) {
    switch (err) {
        case "syntax error":
        case "stack underflow. aborting...":
        case "Stack underflow. aborting...":
            cg.emitError(err);
            break;
        default:
            cg.setErrorPosition();
            break;
    }
}


public Parser(@NotNull Reader r) {
    this.lexer = new Lexer(r, this);
    this.cg = new CodeGenerator(this);

    //this.yydebug = true;
}

public Parser(@NotNull String fileAddress) throws FileNotFoundException {
    this(new FileReader(fileAddress));
    this.fileAddress = fileAddress;
}

//public Parser(Reader r, CodeGenerator cg) {
//    this.lexer = new Lexer(r, this);
//    this.cg = cg;
//
//    //this.yydebug = true;
//}

public Formula parse() {
    cg.parseStatus = this.yyparse();
    if (cg.hasErrors()) {
        throw new RuntimeException("There is some error in input.");
    }
    return cg.getFormula();
}
//#line 346 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 38 "Parser.y"
{ cg.storeFormula(val_peek(0).form); }
break;
case 5:
//#line 46 "Parser.y"
{ cg.declareVariable(val_peek(2).sval, val_peek(0).sval, true); }
break;
case 6:
//#line 47 "Parser.y"
{ cg.declareVariable(val_peek(0).sval, null, true); }
break;
case 10:
//#line 56 "Parser.y"
{ cg.declareVariable(val_peek(2).sval, val_peek(0).sval, false); }
break;
case 11:
//#line 57 "Parser.y"
{ cg.declareVariable(val_peek(0).sval, null, false); }
break;
case 12:
//#line 60 "Parser.y"
{ yyval.term = cg.mkFunctionTerm(Term.FunctionType.PLUS, val_peek(2).term, val_peek(0).term); }
break;
case 13:
//#line 61 "Parser.y"
{ yyval.term = cg.mkFunctionTerm(Term.FunctionType.MINUS, val_peek(2).term, val_peek(0).term); }
break;
case 14:
//#line 62 "Parser.y"
{ yyval.term = cg.mkFunctionTerm(Term.FunctionType.PRODUCT, val_peek(2).term, val_peek(0).term); }
break;
case 15:
//#line 64 "Parser.y"
{ yyval.term = val_peek(1).term; }
break;
case 16:
//#line 65 "Parser.y"
{ yyval.term = val_peek(0).term; }
break;
case 17:
//#line 66 "Parser.y"
{ yyval.term = cg.mkDoubleConstantTerm(val_peek(0).dval); }
break;
case 18:
//#line 69 "Parser.y"
{ yyval.form = cg.mkFormula(Formula.FormulaType.OR, val_peek(2).form, val_peek(0).form); }
break;
case 19:
//#line 70 "Parser.y"
{ yyval.form = cg.mkFormula(Formula.FormulaType.AND, val_peek(2).form, val_peek(0).form); }
break;
case 20:
//#line 71 "Parser.y"
{ yyval.form = cg.mkFormula(Formula.FormulaType.NOT, val_peek(0).form); }
break;
case 21:
//#line 72 "Parser.y"
{ yyval.form = cg.mkPredicate(Formula.PredicateType.EQ, val_peek(2).term, val_peek(0).term); }
break;
case 22:
//#line 73 "Parser.y"
{ yyval.form = cg.mkPredicate(Formula.PredicateType.NE, val_peek(2).term, val_peek(0).term); }
break;
case 23:
//#line 74 "Parser.y"
{ yyval.form = cg.mkPredicate(Formula.PredicateType.LE, val_peek(2).term, val_peek(0).term); }
break;
case 24:
//#line 75 "Parser.y"
{ yyval.form = cg.mkPredicate(Formula.PredicateType.GE, val_peek(2).term, val_peek(0).term); }
break;
case 25:
//#line 76 "Parser.y"
{ yyval.form = cg.mkPredicate(Formula.PredicateType.LT, val_peek(2).term, val_peek(0).term); }
break;
case 26:
//#line 77 "Parser.y"
{ yyval.form = cg.mkPredicate(Formula.PredicateType.GT, val_peek(2).term, val_peek(0).term); }
break;
case 27:
//#line 78 "Parser.y"
{ yyval.form = val_peek(1).form; }
break;
case 28:
//#line 79 "Parser.y"
{ yyval.form = cg.mkBooleanConstantFormula(val_peek(0).bval); }
break;
case 29:
//#line 82 "Parser.y"
{ yyval.term = cg.getDeclaredVariableTerm(val_peek(0).sval); }
break;
//#line 587 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public Parser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
