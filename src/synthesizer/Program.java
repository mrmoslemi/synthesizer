package synthesizer;

import theorem.Formula;
import theorem.TheoremProver;

import java.util.ArrayList;

public class Program {
    private TheoremProver theoremProver;
    private ArrayList<ProgramDecomposition> programDecompositions;
    private Formula completeness;

    public Program(TheoremProver theoremProver) {
        this.theoremProver = theoremProver;
        this.programDecompositions = new ArrayList<>();
        this.completeness = new Formula(Formula.FormulaType.FALSE);
    }

    void addProgramDecomposition(ProgramDecomposition programDecomposition) {
        this.programDecompositions.add(programDecomposition);
        this.completeness = new Formula(Formula.FormulaType.OR, this.completeness, programDecomposition.getPreCondition());
        this.completeness = theoremProver.simplify(this.completeness);
    }

    boolean isComplete() {
        Formula unCompleteness = new Formula(Formula.FormulaType.NOT, this.completeness);
        return !theoremProver.checkSat(unCompleteness);
    }

    Formula getPreConditions() {
        return this.completeness;
    }


    public String toString() {
        StringBuilder toReturn = new StringBuilder();
        for (ProgramDecomposition decomposition : programDecompositions) {
            toReturn.append(decomposition.toString()).append("\n");
        }
        return toReturn.toString();
    }
}
