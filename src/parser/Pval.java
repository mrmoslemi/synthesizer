package parser;

import theorem.Formula;
import theorem.Term;

public class Pval
{
	public enum ValType {
		tpBOOL,
		tpCHAR,
		tpDOUBLE,
		tpFLOAT,
		tpByte,
		tpINT,
		tpLONG,
		tpSTRING,
		tpVOID,
		tpOBJECT,
		tpDottedList,
		tpNULL,
		NO_TYPE;

		public String getText() {
			switch (this) {
				case tpBOOL: return "i1";
				case tpCHAR: return "i8";
				case tpDOUBLE: return "double";
				case tpFLOAT: return "float";
				case tpINT: return "i32";
				case tpByte: return "i8";
				case tpLONG: return "i64";
				case tpSTRING: return "string";
				case tpVOID: return "void";
				case tpOBJECT: return "metadata";
				case tpDottedList: return "...";
				case tpNULL: return "null";
                case NO_TYPE: return "";
				default:
					return "undefined";
			}
		}
	}

	public ValType type;
	public boolean bval;
	public char cval;
	public double dval;
	public float fval;
	public int ival;
	public long lval;
	public String sval;
	public String pref = null;
	public Term term;
	public Formula form;
	public Object obj;

	public Pval(Pval pv) {
		this.type = pv.type;
		this.bval = pv.bval;
		this.cval = pv.cval;
		this.dval = pv.dval;
		this.fval = pv.fval;
		this.ival = pv.ival;
		this.lval = pv.lval;
		this.sval = pv.sval;
		this.pref = pv.pref;
		this.term = pv.term;
		this.form = pv.form;
		this.obj = pv.obj;	// !
	}

	public Pval(boolean val) {
		bval = val;
		type = ValType.tpBOOL;
		sval = "" + val;
	}
	public Pval(char val) {
		cval = val;
		type = ValType.tpCHAR;
		sval = "" + val;
	}
	public Pval(double val) {
		dval = val;
		type = ValType.tpDOUBLE;
		sval = "" + val;
	}
	public Pval(float val) {
		fval = val;
		type = ValType.tpFLOAT;
		sval = "" + val;
	}
	public Pval(int val) {
		ival = val;
		type = ValType.tpINT;
		sval = "" + val;
	}
	public Pval(long val) {
		lval = val;
		type = ValType.tpLONG;
		sval = "" + val;
	}
	public Pval(String val) {
		sval = val;
		type = ValType.tpSTRING;
	}
	public Pval(Term val) {
		term = val;
		type = ValType.NO_TYPE;
		sval = val.toString();
	}
	public Pval(Formula val) {
		form = val;
		type = ValType.NO_TYPE;
		sval = val.toString();
	}
	public Pval(Object val) {
		obj = val;
		type = ValType.tpOBJECT;
		sval = "" + val;
	}
	public Pval() {
		type = ValType.tpNULL;
	}
	public Pval(ValType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		switch (type) {
			case tpBOOL: return (bval ? "true" : "false");
			case tpCHAR:
				switch (cval) {
					case '\\':
						return "'\\" + "\\" + "'";
					case '\"':
						return "'\\" + "\"" + "'";
					case '\r':
						return "'\\" + "r" + "'";
					case '\n':
						return "'\\" + "n" + "'";
					case '\t':
						return "'\\" + "t" + "'";
					default:
						return "'" + cval + "'";
				}
			case tpDOUBLE: return "" + dval;
			case tpFLOAT: return "" + fval;
			case tpByte: return "" + ival;
			case tpINT: return "" + ival;
			case tpLONG: return "" + lval;
			case tpSTRING: return sval;
			case tpOBJECT: return obj.toString();
			case tpDottedList: return "...";
            case NO_TYPE: return "";
			case tpNULL:
			default:
				return " ";
		}
	}

	public Pval self() {
	    return this;
    }
}