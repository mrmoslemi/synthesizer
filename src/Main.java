import synthesizer.Synthesizer;

public class Main {
    public static void main(String[] args) {
        Synthesizer synthesizer = new Synthesizer(args);
        synthesizer.run();
    }
}
