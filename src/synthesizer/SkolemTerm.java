package synthesizer;

import theorem.Term;
import theorem.Variable;


public class SkolemTerm {
    private Variable variable;
    private Term term;

    SkolemTerm(Variable variable, Term term) {
        this.variable = variable;
        this.term = term;
    }

    @Override
    public String toString() {
        return variable.toString() + ":=" + term.toString();
    }
}
