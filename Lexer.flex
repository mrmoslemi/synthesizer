package parser;

%%

//%debug

%class Lexer
%unicode

%byaccj
%integer
%eofval{
	return 0;
%eofval}

%line
%column

%{
	private Parser yyparser;

	public Lexer(java.io.Reader r, Parser p) {
		this(r);
		this.yyparser = p;
	}

	public int getLine() {
		return yyline;
	}

	public int getColumn() {
		return yycolumn;
	}
%}

RInt = [-+]? [0-9]+
RDouble = [-+]? [0-9]+ "." [0-9]+
RInputChar = [^\r\n]	// not line terminator
RComment = "#" {RInputChar}* \R
RInvID = [0-9] ([:jletterdigit:] | ".")*
RID = [:jletter:] [:jletterdigit:]*
RWhiteSpace = ( \R | \s )+

%%

<YYINITIAL> {

    "∧" | "&" | "&&" | "and" { return Parser.AND; }
    "∨" | "|" | "||" | "or" { return Parser.OR; }
    "¬" | "~" | "not" { return Parser.NOT; }
    "→" | "->" | "=>" { return Parser.IMP; }

    "∀" | "forall" { return Parser.FORALL; }
    "∃" | "exists" { return Parser.EXISTS; }

    "⊤" | "true" { yyparser.yylval = new Pval(true); return Parser.TRUE; }
    "⊥" | "false" { yyparser.yylval = new Pval(false); return Parser.FALSE; }

    {RDouble} | {RInt}	{ yyparser.yylval = new Pval(Double.parseDouble(yytext()));
    				        return Parser.cDouble; }

	{RInvID}	{ yyparser.yylval = new Pval(yytext());
				yyparser.cg.emitError("invalid ID " + yytext());
				return Parser.tID; }

	{RID}	{ yyparser.yylval = new Pval(yytext());
				return Parser.tID; }

	{RComment}	{ }
	
	{RWhiteSpace}	{ }

    "=" | "==" { return Parser.EQ; }
	"≠" | "!=" { return Parser.NE; }
	"≤" | "<=" { return Parser.LE; }
	"≥" | ">=" { return Parser.GE; }

	"." | "," | ":" | "<" | ">" | "+" | "-" | "*" | "(" | ")"
	{ yyparser.yylval = new Pval(yycharat(0));
	return (int)yycharat(0); }
	
}

[^]			{ System.err.println(yytext());
			return Parser.eINVALIDTOKEN; }
