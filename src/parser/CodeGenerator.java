package parser;

import theorem.Formula;
import theorem.Term;
import theorem.Variable;

import java.util.HashMap;

public class CodeGenerator {

    public static final String LINE_SEPARATOR = System.lineSeparator();

    private Parser parser;

    private Formula formula;

    public int parseStatus;    // 0 indicates success
    private int errno = 0;  // number of errors occurred during parsing
    private static final int CRITICAL_ERROR_NUMS = 20;  // default 20

    private HashMap<String, Term> variableTerms = new HashMap<String, Term>();

    public CodeGenerator(Parser parser) {
        this.parser = parser;
    }

    void storeFormula(Formula formula) {
        this.formula = formula;
    }

    Formula getFormula() {
        return formula;
    }

    void declareVariable(String variableName, String type, boolean inputVariable) {
        if (type == null) type = "Real";
        if (!type.equals("Real")) emitError("Unknown type: " + type);
        try {
            Variable variable = new Variable(variableName, inputVariable);
            variableTerms.put(variableName, new Term(variable));
        } catch (IllegalArgumentException e) {
            emitError(e.getMessage());
        }
    }

    Term getDeclaredVariableTerm(String variableName) {
        if (variableTerms.containsKey(variableName)) {
            return variableTerms.get(variableName);
        } else {
            emitError(String.format("Variable %s is not declared.", variableName));
            return null;
        }
    }

    Term mkFunctionTerm(Term.FunctionType functionType, Term a, Term b) {
        return new Term(functionType, a, b);
    }

    Formula mkFormula(Formula.FormulaType formulaType, Formula... formulas) {
        return new Formula(formulaType, formulas);
    }

    Formula mkPredicate(Formula.PredicateType predicateType, Term a, Term b) {
        return new Formula(predicateType, a, b);
    }

    Term mkDoubleConstantTerm(double value) {
        return new Term(value);
    }

    Formula mkBooleanConstantFormula(boolean value) {
        return new Formula(value ? Formula.FormulaType.TRUE : Formula.FormulaType.FALSE);
    }

    boolean hasErrors() {
        return errno > 0;
    }

    void emitError(String errtext, int line, int column) {
        errno++;
        if (errtext != null) {
            System.err.println("[" + line + ":" + column + "] " + errtext);
        }
        if (errno >= CRITICAL_ERROR_NUMS) {
            System.err.println("Total " + errno + " errors occurred (critical condition)." + LINE_SEPARATOR + "Aborted.");
            System.exit(1);
        }
    }

    public static final int SRC_LINE_OFFSET = 1, SRC_COLUMN_OFFSET = 1;  // line and column are 0-based

    void emitError(String errtext) {
        emitError(errtext, parser.lexer.getLine() + SRC_LINE_OFFSET, parser.lexer.getColumn() + SRC_COLUMN_OFFSET);
    }

    private int errline = 0, errcolumn = 0;

    void setErrorPosition() {
        errline = parser.lexer.getLine();
        errcolumn = parser.lexer.getColumn();
    }

}
