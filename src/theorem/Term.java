package theorem;

import com.sun.istack.internal.NotNull;

public class Term {
    public enum TermType {
        FUNCTION, CONST, VARIABLE
    }

    public enum FunctionType {
        PLUS, PRODUCT, MINUS, DIVIDE
    }

    private TermType termType;
    private FunctionType functionType;
    private Term[] terms;
    private Variable variable;
    private Double constant;

    public Term(@NotNull Double constant) {
        this.termType = TermType.CONST;
        this.constant = constant;
    }

    public Term(@NotNull Variable variable) {
        this.termType = TermType.VARIABLE;
        this.variable = variable;
    }

    public Term(@NotNull FunctionType functionType, Term... terms) {
        switch (functionType) {
            case PLUS:
            case MINUS:
                if (terms == null || terms.length < 2) {
                    throw new IllegalArgumentException("Illegal number of arguments");
                }
                break;
            case PRODUCT:
            case DIVIDE:
                if (terms == null || terms.length != 2) {
                    throw new IllegalArgumentException("Illegal number of arguments");
                } else {
                    if (!(terms[0].termType.equals(TermType.CONST)) && !(terms[1].termType.equals(TermType.CONST))) {
                        throw new IllegalArgumentException("Production terms can be scalar only");
                    }
                }
                break;
        }
        this.termType = TermType.FUNCTION;
        this.functionType = functionType;
        this.terms = terms;
    }

    public TermType getTermType() {
        return this.termType;
    }

    public FunctionType getFunctionType() {
        return this.functionType;
    }

    public Term[] getTerms() {
        return this.terms;
    }

    public Variable getVariable() {
        return this.variable;
    }

    public Double getConstant() {
        return this.constant;
    }

    @Override
    public String toString() {
        switch (termType) {
            case CONST:
                return constant.toString();
            case VARIABLE:
                return variable.getName();
            case FUNCTION:
                String operator = null;
                StringBuilder toReturn = new StringBuilder("(");
                switch (functionType) {
                    case PLUS:
                        operator = " + ";
                        break;
                    case MINUS:
                        operator = " - ";
                        break;
                    case PRODUCT:
                        operator = " * ";
                        break;
                    case DIVIDE:
                        operator = " / ";
                        break;
                }
                for (int i = 0; i < terms.length; i++) {
                    Term term = terms[i];
                    toReturn.append(term.toString());
                    if (i != terms.length - 1) {
                        toReturn.append(operator);
                    }
                }
                toReturn.append(")");
                return toReturn.toString();
            default:
                return null;
        }
    }

    boolean containsVariable(Variable variable) {
        switch (this.termType) {
            case VARIABLE:
                return this.variable.getName().equals(variable.getName());
            case CONST:
                return false;
            case FUNCTION:
                for (Term subTerm : terms) {
                    if (subTerm.containsVariable(variable)) {
                        return true;
                    }
                }
                return false;
            default:
                throw new IllegalStateException("term type is invalid:" + this.termType);
        }
    }

    double calculateWith(Model model) {
        switch (this.termType) {
            case CONST:
                return this.constant;
            case VARIABLE:
                return model.getValues().get(this.variable);
            case FUNCTION:
                double[] values = new double[terms.length];
                for (int i = 0; i < terms.length; i++) {
                    values[i] = terms[i].calculateWith(model);
                }
                double toReturn = values[0];
                switch (this.functionType) {
                    case PLUS:
                        for (int i = 1; i < values.length; i++) {
                            toReturn += values[i];
                        }
                        return toReturn;
                    case MINUS:
                        for (int i = 1; i < values.length; i++) {
                            toReturn -= values[i];
                        }
                        return toReturn;
                    case PRODUCT:
                        for (int i = 1; i < values.length; i++) {
                            toReturn *= values[i];
                        }
                        return toReturn;
                    case DIVIDE:
                        for (int i = 1; i < values.length; i++) {
                            toReturn /= values[i];
                        }
                        return toReturn;
                    default:
                        throw new IllegalStateException("function type is invalid:" + this.functionType);
                }
            default:
                throw new IllegalStateException("term type is invalid:" + this.termType);
        }
    }
}
