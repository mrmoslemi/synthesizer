# only works with the Java extension of yacc: 
# byacc/j from http://troi.lincom-asg.com/~rjamison/byacc/
# (for Windows, use GnuWin32 make at http://gnuwin32.sourceforge.net/)
# make like this:
#
#   make
#
# or
#
#   make -f [MafefileName] [Target]
#

ifeq ($(OS), Windows_NT)
    WORK_DIR = $(shell cd)
    FORCE_MOVE = move /Y
else
    WORK_DIR = $(shell pwd)
    FORCE_MOVE = mv --reply=yes
endif

# Update PATHs if needed
JFLEX_DIR = $(WORK_DIR)/jflex-1.7.0
JFLEX  = $(JFLEX_DIR)/bin/jflex
BYACCJ_DIR = $(WORK_DIR)/byaccj
BYACCJ = $(BYACCJ_DIR)/yacc
JAVAC  = javac

# targets:

all: build.Lexer build.Parser

build.All: All.class

build.Lexer: Lexer.java moveLexerJava

build.Parser: Parser.java moveParserJava

debug.Parser: debug.Parser.java moveParserJava

All.class: build.Lexer build.Parser
	$(JAVAC) src/parser/Parser.java -sourcepath src/parser -d out/production/synthesizer/

Lexer.java: Lexer.flex
	$(JFLEX) Lexer.flex

Parser.java: Parser.y
	$(BYACCJ) -J -Jpackage=parser -Jclass=Parser -Jsemantic=Pval Parser.y

debug.Parser.java: Parser.y
	$(BYACCJ) -v -J -Jpackage=parser -Jclass=Parser -Jsemantic=Pval Parser.y

moveParserJava:
	$(FORCE_MOVE) Parser.java "src/parser/"

moveLexerJava:
	$(FORCE_MOVE) Lexer.java "src/parser/"
